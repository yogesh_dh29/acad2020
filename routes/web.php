<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('homepage.index'); });
Route::get('admin', function () { return view('admin_template'); });
Route::get('indexx', function () { return view('indexx'); });
Route::get('/homepage', function (){ return view('homepage.index'); })->name('homepage.index');

Route::get('dl', 'DL\RegistrationsController@home')->name('dl.home');
Route::any('dl/registrations', 'DL\RegistrationsController@index')->name('dl.index');
Route::get('dl/registrations/variants', 'DL\RegistrationsController@getVariants')->name('dl.variants');
Route::get('dl/registrations/instances', 'DL\RegistrationsController@getInstances')->name('dl.instances');

Route::any('dl/registrations/filter', 'DL\RegistrationsController@index')->name('dl.filter');
Route::any('dl/registration', 'DL\RegistrationsController@indexx');
Route::any('dl/registrationn', 'DL\RegistrationsController@indexxx')->name('dl.indexxx');
Route::post('dl/registrationn/', 'DL\RegistrationsController@indexxx')->name('dl.search');
// Route::post('dl/registrationn/search/{type}', 'DL\RegistrationsController@indexxx')->name('dl.search.pending');

Route::get('dl/courses', 'DL\CoursesController@index')->name('dl.courses.index');
Route::any('dl/courses/filter', 'DL\CoursesController@index')->name('dl.courses.filter');


//Route::any('dl/registrationn/{type}', 'DL\RegistrationsController@indexxx')->name('dl.reg.pending');


Route::any('test', 'TestController@index');

//System Settings Module Routes
Route::get('/systemsettings','SystemSettings\SystemSettingsController@index')->name('system_settings.index');
Route::get('/systemsettings/program/index','SystemSettings\Program\ProgramsController@index')->name('program.index');
Route::get('/systemsettings/program/create','SystemSettings\Program\ProgramsController@create')->name('program.create');
