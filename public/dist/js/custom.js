//Side bar menu search
function searchMenu() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("q");
    filter = input.value.toUpperCase();
    ul = document.getElementById("sidebar-menu");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i];//.getElementsByTagName("a")[0];
        //console.log(a);
        if (a.innerText.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}

function unsetFilter(key) {
    var element = key.replace('_unset','');
    // Reset select dropdowns
    $('select[name='+element+'], input[name='+element+']').removeClass('selected-filters')
    $('select[name='+element+'] option')
    .removeAttr('selected')
    .find(':first')     
    .attr('selected','selected');
    // Reset input
    $('input[name='+element+']').val('');
    $('#'+element).prop('checked', false);
    $('#'+key).val(1);
    $('#id_'+element).hide();
}