@extends('layouts.app')


@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DL Registrations
        <!-- <small>advanced tables</small> -->                
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('homepage.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">DL Registrations</li>
      </ol>
    </section>
        <!-- Main content -->
    <section class="content">           
            <div class="box">
            <!-- <div class="box-header">
            <h3 class="box-title">DL Registrations</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- SELECT2 EXAMPLE -->
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Search Filters</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <!-- /.col -->
                  <div class="col-md-2">
                    <div class="form-group">
                     <input name="txtsearch" class="form-control" size="20" type="text" value="" placeholder="Enter space separated text" />
                    </div>
                    <!-- /.form-group -->
                  </div> 
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="year" id="year" class="form-control select2" multiple="multiple" data-placeholder=" « Instance Year » " style="width: 100%;">
                          <option value="">« Instance Year »</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                      <select name="language" id="language" class="form-control select2" multiple="multiple" data-placeholder=" « Language » " style="width: 100%;">
                        <option value="">« Language »</option>
                      @foreach($language as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                      @endforeach
                      </select>                                   
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                      <select name="course_acronym" id="course_acronym" class="form-control select2" multiple="multiple" data-placeholder=" « Course » " style="width: 100%;" onchange="getVariants(this.value)">
                        <option value="">« Course »</option>
                          @foreach($onebigcourse as $c)
                            @foreach($c->courses as $co)
                              <option value="{{ $co->acronym }}">{{ $co->acronym }}</option>
                            @endforeach
                          @endforeach                        
                      </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="course_variants" class="form-control select2" id="course_variants" multiple="multiple" data-placeholder=" « Variants » " style="width: 100%;" onchange="getInstances(this.value);">
                          <option value="">« Variants »</option>
                            @foreach($onebigcourse as $c)
                              @foreach($c->courses as $co)
                                @foreach($co->variants as $va)
                                  <option value="{{ $va->acronym }}">{{ $va->acronym }}</option>
                                @endforeach
                              @endforeach
                            @endforeach                                                              
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="course_code" class="form-control select2" multiple="multiple" data-placeholder=" « Instances » " style="width: 100%;" id="course_code">
                          <option value="">« Instances »</option>
                            @foreach($onebigcourse as $c)
                              @foreach($c->courses as $co)
                                @foreach($co->variants as $va)                                
                                  @foreach($va->instances as $ins)
                                    <option value="{{ $ins->acronym }}">{{ $ins->acronym }}</option>
                                  @endforeach
                                @endforeach
                              @endforeach
                            @endforeach

                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>                                  
                </div>
                <!-- /.row -->
                <div class="row">
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="country" id="country" class="form-control select2" multiple="multiple" data-placeholder=" « Country » " style="width: 100%;">
                          <option value="">« Country »</option>
                            @foreach($country as $id => $name)
                              <option value="{{ $id}}"> {{ $name }} </option>
                            @endforeach  
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="nationality" id="nationality" class="form-control select2" multiple="multiple" data-placeholder=" « Nationality » " style="width: 100%;">
                          <option value="">« Nationality »</option>
                            @foreach($nationality as $id => $name)
                              <option value="{{ $id}}"> {{ $name }} </option>
                            @endforeach 
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                      <div class="form-group">
                          <select name="bureau" id="bureau" class="form-control select2" multiple="multiple" data-placeholder=" « Bureau » " style="width: 100%;">
                            <option value="">« Bureau »</option>
                                @foreach($bureau as $id => $name)
                                <option value="{{ $id}}"> {{ $name }} </option>
                                @endforeach 
                          </select>
                      </div>
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="eco_group" id="eco_group" class="form-control select2" multiple="multiple" data-placeholder=" « Eco Group » " style="width: 100%;">
                            <option value="">« Eco Group »</option>
                              @foreach($eco_group as $id => $name)
                              <option value="{{ $id}}"> {{ $name }} </option>
                              @endforeach                                                       
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="gender" id="gender" class="form-control">
                          <option value="">« Gender »</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="occupation" id="occupation" class="form-control">
                          <option value="">« Occupation »</option>
                          <option value="student">Student</option>
                          <option value="professional">Professional</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>                
                </div>
                <!-- /.row -->
                <div class="row">
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="occupation" id="occupation" class="form-control select2" multiple="multiple" data-placeholder=" « Prof Area » " style="width: 100%;">
                          <option value="">« Prof Area »</option>
                            @foreach($professional_area as $professional_areas)
                              @foreach($professional_areas->professional_areas as $profarea)
                                <option value="{{ $profarea->id }}">{{ $profarea->name_en }}</option>
                              @endforeach
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                        <select name="confirmation" id="confirmation" class="form-control">
                          <option value="">« Confirmation »</option>
                          <option value="Paying">Paying</option>
                          <option value="Free">Free</option>
                        </select>
                    </div>
                     <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                        <select name="payment_method" id="payment_method" class="form-control">
                          <option value="">« Pay Method »</option>
                          <option value="transfer">Bank Transfer</option>
                          <option value="creditcard">Credit Card</option>
                          <option value="wipoacc">WIPO Account</option>
                          <option value="scholarship">Scholarship</option>
                          <option value="offered">WIPO Offered</option>
                        </select>
                    </div>
                     <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                  <div class="form-group">
                      <select name="cmo" id="cmo" class="form-control">
                        <option value="">« CMO »</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                      </select>
                  </div>
                   <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                  <div class="form-group">
                    <select name="is_ext_reg" id="is_ext_reg" class="form-control">
                      <option value="">« Ext Reg. »</option>
                      <option value="No">Regular Registrations</option>
                      <option value="Yes">External Registrations</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                  </div>
                  <div class="col-md-1">
                    <div class="form-group">
                      <div class="checkbox">
                        <label style="font-size: 12px;">
                          <input type="checkbox" checked="checked"> + UPOV
                        </label>
                      </div>                                          
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div class="form-group">
                      <div class="checkbox">
                        <label style="font-size: 12px;">
                          <input type="checkbox" checked="checked"> + JTIP
                        </label>
                      </div>                                          
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="box-body">
                <table id="example2" class="table table-bordered table-hover" data-page-length='50'>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Course</th>
                      <th>Paying</th>
                      <th>Admin</th>
                      <th>Currency</th>
                      <th>Course Type</th>
                      <th>Open</th>
                      <th>Special</th>
                      <th>CMO</th>
                      <th>Duration</th>
                      <th>Certificate</th>
                      <th>Series</th>
                      <th>IP Field</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Course</th>
                      <th>Paying</th>
                      <th>Admin</th>
                      <th>Currency</th>
                      <th>Course Type</th>
                      <th>Open</th>
                      <th>Special</th>
                      <th>CMO</th>
                      <th>Duration</th>
                      <th>Certificate</th>
                      <th>Series</th>
                      <th>IP Field</th>
                    </tr>
                  </tfoot>                
                  <tbody>
                  </tbody>
                </table>
              </div>
            <!-- /.box-body -->
            </div>
    </section>

@endsection

@section('javascripts')

  <!-- jQuery 3 -->
  <script src={{ asset("bower_components/jquery/dist/jquery.min.js") }}></script>
  <!-- Bootstrap 3.3.7 -->
  <script src={{ asset("bower_components/bootstrap/dist/js/bootstrap.min.js") }}></script>
  <!-- DataTables -->
  <script src={{ asset("bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
  <script src={{ asset("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
  <!-- SlimScroll -->
  <script src={{ asset("bower_components/jquery-slimscroll/jquery.slimscroll.min.js") }}></script>
  <!-- FastClick -->
  <script src={{ asset("bower_components/fastclick/lib/fastclick.js") }}></script>
  <!-- AdminLTE App -->
  <script src={{ asset("dist/js/adminlte.min.js") }}></script>
  <!-- AdminLTE for demo purposes -->
  <script src={{ asset("dist/js/demo.js") }}></script>
  <!-- Select2 -->
  <script src={{ asset("bower_components/select2/dist/js/select2.full.min.js") }}></script>
  <!-- InputMask -->
  <script src={{ asset("plugins/input-mask/jquery.inputmask.js") }}></script>
  <script src={{ asset("plugins/input-mask/jquery.inputmask.date.extensions.js") }}></script>
  <script src={{ asset("plugins/input-mask/jquery.inputmask.extensions.js") }}></script>
  <!-- date-range-picker -->
  <script src={{ asset("bower_components/moment/min/moment.min.js") }}></script>
  <script src={{ asset("bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
  <!-- bootstrap time picker -->
  <script src={{ asset("plugins/timepicker/bootstrap-timepicker.min.js") }}></script>
  <!-- bootstrap datepicker -->
  <script src={{ asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
  <!-- iCheck 1.0.1 -->
  <script src={{ asset("plugins/iCheck/icheck.min.js") }}></script>
  <!-- bootstrap color picker -->
  <script src={{ asset("bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js") }}></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/select.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
      maximumSelectionLength: 1
    });
});
</script>
<script type="text/javascript">
$(document).ready( function () {
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

    var table = $('#example2').DataTable({
        processing: true,
        searching: false,
        lengthChange: false,
        serverSide: true,
        paging: true,
        responsive: true,
        scrollX: true,
        autoWidth: false,
        sPaginationType: 'listbox',
        order: [1,'desc'],
        ajax: {
          url: "{{ route('dl.courses.filter') }}",
          type: 'GET',
         },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'acronym', name: 'acronym' },
            { data: 'is_paying', name: 'is_paying' },
            { data: 'admin_email', name: 'admin_email' },
            { data: 'currency_code', name: 'currency_code' },
            { data: 'course_type', name: 'course_type' },
            { data: 'is_open', name: 'is_open' },
            { data: 'is_special', name: 'is_special' },
            { data: 'is_cmo_enabled', name: 'is_cmo_enabled' },
            { data: 'duration', name: 'duration' },
            { data: 'has_certificate', name: 'has_certificate' },
            { data: 'series', name: 'series' },
            { data: 'ip_field', name: 'ip_field' },
        ]
    });
});

$('select').change(function (){
  $('#example2').DataTable().draw(true);
});
</script>  
<script>
  function searchMenu() {
      var input, filter, ul, li, a, i;
      input = document.getElementById("q");
      filter = input.value.toUpperCase();
      ul = document.getElementById("sidebar-menu");
      li = ul.getElementsByTagName("li");
      for (i = 0; i < li.length; i++) {
          a = li[i];//.getElementsByTagName("a")[0];
          //console.log(a);
          if (a.innerText.toUpperCase().indexOf(filter) > -1) {
              li[i].style.display = "";
          } else {
              li[i].style.display = "none";

          }
      }
  }
</script>
@endsection

