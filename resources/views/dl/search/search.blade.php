<div class="box-body" style="">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>Keyword</label>
       <input name="txtsearch" class="form-control" size="20" type="text" value="" placeholder="Enter space separated text" />
      </div>
      <!-- /.form-group -->
    </div> 
    <div class="col-md-2">  
      <div class="form-group">
        <label>Instance</label>
          <select name="year" id="year" class="form-control">
            <option value="">« Instance Year »</option>
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Language</label>
        <select name="language" class="form-control">
          <option value="">« Language »</option>
        @foreach($language as $id => $name)
          <option value="{{ $id }}">{{ $name }}</option>
        @endforeach
        </select>                                   
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Course</label>
        <select name="course_acronym" class="form-control" onchange="getVariants(this.value)">
          <option value="">« Course »</option>
            @foreach($course as $c)
              @foreach($c->courses as $co)
                <option value="{{ $co->acronym }}">{{ $co->acronym }}</option>
              @endforeach
            @endforeach                        
        </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Variants</label>
          <select name="course_variants" class="form-control" id="course_variants" onchange="getInstances(this.value);">
            <option value="">« Variants »</option>
              @foreach($variant as $v)
                @foreach($v->variants as $va)
                  <option value="{{ $va->acronym }}">{{ $va->acronym }}</option>
                @endforeach
              @endforeach                                                            
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Instances</label>
          <select name="course_code" class="form-control" id="course_code">
            <option value="">« Instances »</option>
              @foreach($instance as $in)
                @foreach($in->variants as $var)
                  @foreach($var->instances as $ins)
                    <option value="{{ $ins->acronym }}">{{ $ins->acronym }}</option>
                  @endforeach
                @endforeach
              @endforeach      
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Country</label>
          <select name="country" class="form-control">
            <option value="">« Country »</option>
              @foreach($country as $id => $name)
                <option value="{{ $id}}"> {{ $name }} </option>
              @endforeach  
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Nationality </label>
          <select name="nationality" class="form-control">
            <option value="">« Nationality »</option>
              @foreach($nationality as $id => $name)
                <option value="{{ $id}}"> {{ $name }} </option>
              @endforeach 
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Bureau</label>
          <select name="bureau" class="form-control">
            <option value="">« Bureau »</option>
             @foreach($bureau as $id => $name)
                <option value="{{ $id}}"> {{ $name }} </option>
            @endforeach 
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Eco Group</label>
          <select name="eco_group" class="form-control">
              <option value="">« Eco Group »</option>
                @foreach($eco_group as $id => $name)
                <option value="{{ $id}}"> {{ $name }} </option>
                @endforeach                                                       
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Gender</label>
          <select name="gender" class="form-control">
            <option value="">« Gender »</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Occupation</label>
          <select name="occupation" class="form-control">
            <option value="">« Occupation »</option>
            <option value="student">Student</option>
            <option value="professional">Professional</option>
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">  
      <div class="form-group">
        <label>Prof Area</label>
          <select name="occupation" class="form-control">
            <option value="">« Prof Area »</option>
              @foreach($professional_area as $professional_areas)
                @foreach($professional_areas->professional_areas as $profarea)
                  <option value="{{ $profarea->id }}">{{ $profarea->name_en }}</option>
                @endforeach
              @endforeach
          </select>
      </div>
      <!-- /.form-group -->
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label for="">Confirmation</label>
          <select name="confirmation" class="form-control">
            <option value="">« Confirmation »</option>
            <option value="Paying">Paying</option>
            <option value="Free">Free</option>
          </select>
      </div>
       <!-- /.form-group -->
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label for="">Pay Method</label>
          <select name="payment_method" class="form-control">
            <option value="">« Pay Method »</option>
            <option value="transfer">Bank Transfer</option>
            <option value="creditcard">Credit Card</option>
            <option value="wipoacc">WIPO Account</option>
            <option value="scholarship">Scholarship</option>
            <option value="offered">WIPO Offered</option>
          </select>
      </div>
       <!-- /.form-group -->
    </div>
    <div class="col-md-2">
    <div class="form-group">
      <label for="">CMO</label>
        <select name="cmo" class="form-control">
          <option value="">« CMO »</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
    </div>
     <!-- /.form-group -->
    </div>
    <div class="col-md-2">
    <div class="form-group">
    <label for="">« Ext Reg. »</label>
      <select name="is_ext_reg" class="form-control">
        <option value="">« Ext Reg. »</option>
        <option value="No">Regular Registrations</option>
        <option value="Yes">External Registrations</option>
      </select>
    </div>
    <!-- /.form-group -->
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <div class="checkbox">
          <label>
            <input type="checkbox" checked="checked"> + UPOV
          </label>
        </div>                                          
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <div class="checkbox">
          <label>
            <input type="checkbox" checked="checked"> + JTIP
          </label>
        </div>                                          
      </div>
    </div>
    <div class="col-md-2">  
      <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
      </div>
    </div>
  <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<!-- /.box-body -->