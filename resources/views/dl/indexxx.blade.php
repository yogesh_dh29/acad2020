@extends('layouts.app')

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DL Registrations
        <!-- <small>advanced tables</small> -->                
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('homepage.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('dl.home')}}">DL</a></li>
        <li class="active">Registrations</li>
      </ol>
    </section>
        <!-- Main content -->
    <section class="content"> 
      <form action="{{ route('dl.search') }}" method="post" name="search-filter" id="search-filter">
      @csrf
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Search filters</h3>
                <hr></hr>
                <!-- END ALERTS AND CALLOUTS -->
                <!-- START CUSTOM TABS -->
                <div class="row">
                  <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs" id="myTab">  
                        <li><a href="RenderPending" class="a" data-toggle="tab">Pending</a></li>
                        <li><a href="RenderConfirmed" class="a" data-toggle="tab">Confirmed</a></li>
                        <li><a href="RenderCancelled" class="a" data-toggle="tab">Cancelled</a></li>
                        <li><a href="RenderAllCurrent" class="a" data-toggle="tab">All Current</a></li>
                        <li><a href="RenderAll" class="a" data-toggle="tab">All</a></li>
                        <li><a href="RenderExpired" class="a" data-toggle="tab">Expired</a></li>
                        <input type="hidden" id="registration_type" name="registration_type" />
                      </ul>
                    </div>
                    <!-- nav-tabs-custom -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- END CUSTOM TABS -->
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="">
                <div class="row">
{{--                   <div class="col-md-2">
                    <div class="form-group">
                     <input name="txtsearch" class="form-control" size="20" type="text" value="" placeholder="Enter space separated text" />
                    </div>
                    <!-- /.form-group -->
                  </div> 
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="year" id="year" class="form-control">
                          <option value="">« Instance Year »</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div> --}}
                  <div class="col-md-2">  
                    <div class="form-group">
                      <select name="language" id="language" class="form-control">
                        <option value="">« Language »</option>
                        @foreach($language as $id => $name)
                          <option value="{{ $id }}" {{ Request::input('language') == $id ? 'selected="selected"' : ''}}>{{ $name }}</option>
                        @endforeach
                      </select>                                   
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                      <select name="course_acronym" id="course_acronym" class="form-control">
                        <option value="">« Course »</option>
                          @foreach($onebigcourse as $c)
                            @foreach($c->courses as $co)
                              <option value="{{ $co->acronym }}" {{ Request::input('course_acronym') == $co->acronym ? 'selected="selected"' : ''}}>{{ $co->acronym }}</option>
                            @endforeach
                          @endforeach                        
                      </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="course_variants" class="form-control" id="course_variants">
                          <option value="">« Variants »</option>
                            @foreach($onebigcourse as $c)
                              @foreach($c->courses as $co)
                                @foreach($co->variants as $va)
                                  <option value="{{ $va->acronym }}" {{ Request::input('course_variants') == $va->acronym ? 'selected="selected"' : ''}}>{{ $va->acronym }}</option>
                                @endforeach
                              @endforeach
                            @endforeach                                                           
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="course_code" class="form-control" id="course_code">
                          <option value="">« Instances »</option>
                            @foreach($onebigcourse as $c)
                              @foreach($c->courses as $co)
                                @foreach($co->variants as $va)                                
                                  @foreach($va->instances as $ins)
                                    <option value="{{ $ins->acronym }}" {{ Request::input('course_code') == $ins->acronym ? 'selected="selected"' : ''}}>{{ $ins->acronym }}</option>
                                  @endforeach
                                @endforeach
                              @endforeach
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="country" id="country" class="form-control">
                          <option value="">« Country »</option>
                            @foreach($country as $id => $name)
                              <option value="{{ $id }}" {{ Request::input('country') == $id ? 'selected="selected"' : ''}}> {{ $name }} </option>
                            @endforeach  
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="nationality" id="nationality" class="form-control">
                          <option value="">« Nationality »</option>
                            @foreach($nationality as $id => $name)
                              <option value="{{ $id}}" {{ Request::input('nationality') == $id ? 'selected="selected"' : ''}}> {{ $name }} </option>
                            @endforeach 
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                      <div class="form-group">
                          <select name="bureau" id="bureau" class="form-control">
                            <option value="">« Bureau »</option>
                                @foreach($bureau as $id => $name)
                                <option value="{{ $id}}" {{ Request::input('bureau') == $id ? 'selected="selected"' : ''}}> {{ $name }} </option>
                                @endforeach 
                          </select>
                      </div>
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="eco_group" id="eco_group" class="form-control">
                            <option value="">« Eco Group »</option>
                              @foreach($eco_group as $id => $name)
                              <option value="{{ $id}}" {{ Request::input('eco_group') == $id ? 'selected="selected"' : ''}}> {{ $name }} </option>
                              @endforeach                                                       
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="gender" id="gender" class="form-control">
                          <option value="">« Gender »</option>
                          <option value="Male"  {{ Request::input('gender') == 'Male' ? 'selected="selected"' : ''}}>Male</option>
                          <option value="Female"{{ Request::input('gender') == 'Female' ? 'selected="selected"' : ''}}>Female</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="occupation" id="occupation" class="form-control">
                          <option value="">« Occupation »</option>
                          <option value="student">Student</option>
                          <option value="professional">Professional</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">  
                    <div class="form-group">
                        <select name="prof_area" id="prof_area" class="form-control">
                          <option value="">« Prof Area »</option>
                          {{ print_r(Request::input('prof_area')) }}
                            @foreach($professional_area as $professional_areas)
                              @foreach($professional_areas->professional_areas as $profarea)
                                <option value="{{ $profarea->id }}">{{ $profarea->name_en }}</option>
                              @endforeach
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                        <select name="confirmation" id="confirmation" class="form-control">
                          <option value="">« Confirmation »</option>
                          {{ print_r(Request::input('confirmation')) }}
                          <option value="Paying">Paying</option>
                          <option value="Free">Free</option>
                        </select>
                    </div>
                     <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                        <select name="payment_method" id="payment_method" class="form-control">
                          <option value="">« Pay Method »</option>
                          {{ print_r(Request::input('payment_method')) }}
                          <option value="transfer">Bank Transfer</option>
                          <option value="creditcard">Credit Card</option>
                          <option value="wipoacc">WIPO Account</option>
                          <option value="scholarship">Scholarship</option>
                          <option value="offered">WIPO Offered</option>
                        </select>
                    </div>
                     <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                  <div class="form-group">
                      <select name="cmo" id="cmo" class="form-control">
                        <option value="">« CMO »</option>
                        {{ print_r(Request::input('cmo')) }}
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                      </select>
                  </div>
                   <!-- /.form-group -->
                  </div>
                  <div class="col-md-2">
                  <div class="form-group">
                    <select name="is_ext_reg" id="is_ext_reg" class="form-control">
                      <option value="">« Ext Reg. »</option>
                      <option value="No">Regular Registrations</option>
                      <option value="Yes">External Registrations</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                  </div>
                  <div class="col-md-1">
                    <div class="form-group">
                      <div class="checkbox">
                        <label style="font-size: 12px;">
                          <input type="checkbox" checked="checked"> + UPOV
                        </label>
                      </div>                                          
                    </div>
                  </div>
                  <div class="col-md-1">
                    <div class="form-group">
                      <div class="checkbox">
                        <label style="font-size: 12px;">
                          <input type="checkbox" checked="checked"> + JTIP
                        </label>
                      </div>                                          
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row row-clearfilter">
                    @if(!empty(Request::all()))
                      @foreach(array_diff_key(Request::all(),[ "_token" => "xy"]) as $key => $value)
                        @if(!empty($value))
                        <button type="button" class="btn bg-navy margin btn-sm clearfilter search-bubble" id="bubble_{{ $key }}" onclick="Reset('{{ $key }}')">
                        @php
                          echo ucfirst(str_replace("_", " ", $key)); 
                        @endphp &nbsp;
                          <i class="fa fa-times"></i>
                        </button>                   
                        @endif  
                      @endforeach
                    @endif                          
                    </div>                    
                  </div>
                  <div class="col-md-12">  
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search/Refresh</button>
                    </div>
                  </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
            </div>
      </form>            
            <div class="box">
            <!-- <div class="box-header">
            <h3 class="box-title">DL Registrations</h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover" data-page-length='50'>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Instance</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Prof. Area</th>
                    <th>CTR</th>
                    <th>NAT</th>
                    <th>PM</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $RpDlUpovRegistrationView)
                  <tr>
                    <td>{{ $RpDlUpovRegistrationView->reg_id }}</td>
                    <td>{{ $RpDlUpovRegistrationView->reg_date_create }}</td>
                    <td>{{ $RpDlUpovRegistrationView->ci_acronym }}</td>
                    <td>{{ $RpDlUpovRegistrationView->client_full_name }}</td>
                    <td>{{ $RpDlUpovRegistrationView->client_email }}</td>
                    <td>{{ $RpDlUpovRegistrationView->client_occupation }}</td>
                    <td>{{ $RpDlUpovRegistrationView->reg_country }}</td>
                    <td>{{ $RpDlUpovRegistrationView->reg_nationality }}</td>
                    <td>{{ $RpDlUpovRegistrationView->ci_passing_grades }}</td>
                    <td>{{ $RpDlUpovRegistrationView->reg_currency_code }}</td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Instance</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Prof. Area</th>
                    <th>CTR</th>
                    <th>NAT</th>
                    <th>PM</th>
                    <th>Amount</th>
                  </tr>
                </tfoot>
                <tbody>
                </tbody>
              </table>
                <div class="row">
                  <div class="col-6" style="display: flex;margin-left: 15px;padding-top: 5px;">
                    Showing {{ $data->count() }} of {{ $data->total() }}
                  </div>
                </div>
                <div class="row">
                  <div class="col-12" style="display: flex;justify-content: center;">
                    {{ $data->appends($d)->links() }}
                  </div>
                </div>                
            </div>
            <!-- /.box-body -->
            </div>
        </div>  
    </section>

@endsection

@section('javascripts')

  <!-- jQuery 3 -->
  <script src={{ asset("bower_components/jquery/dist/jquery.min.js") }}></script>
  <!-- Bootstrap 3.3.7 -->
  <script src={{ asset("bower_components/bootstrap/dist/js/bootstrap.min.js") }}></script>
  <!-- DataTables -->
  <script src={{ asset("bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
  <script src={{ asset("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
  <!-- SlimScroll -->
  <script src={{ asset("bower_components/jquery-slimscroll/jquery.slimscroll.min.js") }}></script>
  <!-- FastClick -->
  <script src={{ asset("bower_components/fastclick/lib/fastclick.js") }}></script>
  <!-- AdminLTE App -->
  <script src={{ asset("dist/js/adminlte.min.js") }}></script>
  <!-- AdminLTE for demo purposes -->
  <script src={{ asset("dist/js/demo.js") }}></script>
  <!-- Select2 -->
  <script src={{ asset("bower_components/select2/dist/js/select2.full.min.js") }}></script>
  <!-- InputMask -->
  <script src={{ asset("plugins/input-mask/jquery.inputmask.js") }}></script>
  <script src={{ asset("plugins/input-mask/jquery.inputmask.date.extensions.js") }}></script>
  <script src={{ asset("plugins/input-mask/jquery.inputmask.extensions.js") }}></script>
  <!-- date-range-picker -->
  <script src={{ asset("bower_components/moment/min/moment.min.js") }}></script>
  <script src={{ asset("bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
  <!-- bootstrap time picker -->
  <script src={{ asset("plugins/timepicker/bootstrap-timepicker.min.js") }}></script>
  <!-- bootstrap datepicker -->
  <script src={{ asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
  <!-- iCheck 1.0.1 -->
  <script src={{ asset("plugins/iCheck/icheck.min.js") }}></script>
  <!-- bootstrap color picker -->
  <script src={{ asset("bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js") }}></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/pagination/select.js"></script> 
<script type="text/javascript">
$(document).ready( function () {
    var tabID; 
    $(".a").on("click", function() {     
      tabID = $(this).attr('href');
      $("#registration_type").val(tabID); 
    });
    var tab = "{{ Request::get('registration_type') }}";

    $("#myTab").find("a").each(function(){
      var actual = $(this).attr("href");
      if(actual == tab)
      {
        $(this).parent("li").addClass("active");
      }            
    });

    $("#course_acronym").change(function(){
      var ca = $(this).val();
      if(ca){
        $.ajax({
          type: "GET",
          url: "{{ route('dl.variants') }}",
          data: {acronym:ca},
          success:function(res){
            if(res){
              console.log(res);
              $("#course_variants").empty();
              $("#course_variants").append('<option>« Variants »</option>');              
              $.each(res, function(key,value){
                    $("#course_variants").append('<option value="'+value+'">'+value+'</option>');           
              });
            }
          }
        });
      }
      else{
        $.ajax({
          type: "GET",
          url: "{{ route('dl.variants') }}",
          data: {acronym:ca},
          success:function(res){
            if(res){
              $("#course_variants").empty();
              $("#course_variants").append('<option>« Variants »</option>');              
              $.each(res, function(key,value){
                $.each(value.courses, function(keyy,valuee){
                  $.each(valuee.variants, function(k,v){
                    $("#course_variants").append('<option value="'+v.acronym+'">'+v.acronym+'</option>');
                  });                 
                });
              });
            }
          }
        });
      }
    });
    $("#course_variants").change(function(){
      var ca = $(this).val();
      if(ca == "« Variants »"){
        $.ajax({
          type: "GET",
          url: "{{ route('dl.instances') }}",
          data: {variant:ca},          
          success:function(res){
            if(res){
              $("#course_code").empty();
              $("#course_code").append('<option value>« Instances »</option>');              
              $.each(res, function(key,value){
                $.each(value.courses, function(keyy,valuee){
                  $.each(valuee.variants, function(k,v){
                    $.each(v.instances, function(ke,va){
                    $("#course_code").append('<option value="'+va.acronym+'">'+va.acronym+'</option>');                      
                    });
                  });                 
                });
              });
            }
          }
        });                
      }
      else
      {
        $.ajax({
          type: "GET",
          url: "{{ route('dl.instances') }}",
          data: {variant:ca},          
          success:function(res){
            if(res){
              $("#course_code").empty();
              $("#course_code").append('<option>« Instances »</option>');              
              $.each(res, function(key,value){
                    $("#course_code").append('<option value="'+value+'">'+value+'</option>');           
              });
            }
          }
        });
      }
    });
  });
</script>
<script type="text/javascript">
    function Reset(dropdown_value) {      
      if(dropdown_value == "registration_type")
      {
        document.querySelector("#myTab > li.active").classList.remove("active");
      }
      else
      {
        var dropDown = document.getElementById(dropdown_value);
        dropDown.selectedIndex = 0;      
      }
      var myobj = document.getElementById("bubble_"+dropdown_value);
      myobj.remove();
    }
</script>
<script>
  $(function () {
    $('#example1').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false,      
    })
  })
</script>
  <script>
    function searchMenu() {
        var input, filter, ul, li, a, i;
        input = document.getElementById("q");
        filter = input.value.toUpperCase();
        ul = document.getElementById("sidebar-menu");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i];//.getElementsByTagName("a")[0];
            //console.log(a);
            if (a.innerText.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";

            }
        }
    }
  </script>
@endsection

