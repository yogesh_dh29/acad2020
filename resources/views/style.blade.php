<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href={{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}>
  <!-- Font Awesome -->
  <link rel="stylesheet" href={{ asset("bower_components/font-awesome/css/font-awesome.min.css") }}>
  <!-- Ionicons -->
  <link rel="stylesheet" href={{ asset("bower_components/Ionicons/css/ionicons.min.css") }}>

  <!-- Morris chart -->
  <link rel="stylesheet" href={{ asset("bower_components/morris.js/morris.css") }}>
  <!-- jvectormap -->
  <link rel="stylesheet" href={{ asset("bower_components/jvectormap/jquery-jvectormap.css") }}>
  <!-- Date Picker -->
  <link rel="stylesheet" href={{ asset("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
  <!-- Daterange picker -->
  <link rel="stylesheet" href={{ asset("bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
   <!-- Select2 -->
  <link rel="stylesheet" href={{ asset("bower_components/select2/dist/css/select2.min.css") }}>

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href={{ asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") }}>
  <!-- DataTables -->
  <link rel="stylesheet" href= {{ asset("bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
  <!-- Theme style -->
  <link rel="stylesheet" href={{ asset("dist/css/AdminLTE.min.css") }}>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href={{ asset("dist/css/skins/_all-skins.min.css") }}>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
  .clearfilter{
    margin-left: 10px;
    margin-right: 0px;
    margin-bottom: 20px;  
  }  
  .row-clearfilter{
    margin-left: auto;
  }
  .bg-dl{
    background-color: #A72B30 !important;
    color: white
  } 
  .bg-upov {
    background-color: #61721C !important;
    color: white
  }
  .bg-pct {
    background-color: #7dd2ca !important;
    color: black    
  }
  .bg-pct:hover {
    text-decoration: none;
    color: black;
  }
  .bg-ip4youth {
    background-color: #7dd2ca !important;
    color: black
  }
  .bg-ip4youth:hover {
    text-decoration: none;
    color: black;
  }
  .bg-pdp {
    background-color: #00AEEF !important;
    color: white
  }
  .bg-aip {
    background-color: #253F8E !important;
    color: white
  }
  .bg-wss {
    background-color: #CB1987 !important;
    color: white
  }
  .bg-block_information {
    background-color: #7dd2ca;
    color: black
  }
  .bg-block_information:hover {
    text-decoration: none;
    color: black;
  }
  .select:valid {
    border-bottom: 2px solid #ff5722;
  }
  </style>
