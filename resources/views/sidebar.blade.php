  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src={{ asset("dist/img/user2-160x160.jpg") }} class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Dilshad Khan</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="{{ route('homepage.index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>Distance Learning Program</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!--             <li><a href="dl-registration.html"><i class="fa fa-id-card-o"></i> Registrations</a></li> -->
            <li><a href="{{ route('dl.indexxx') }}"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-graduation-cap"></i> Scholarships</a></li>
            <li><a href="#"><i class="fa fa-ticket"></i> Discount Codes</a></li>
            <li><a href="#"><i class="fa fa-users"></i> DL Participants</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-star-half-o"></i> Completion Report</a></li>
            <li><a href="#"><i class="fa fa-certificate"></i> Certificate Tracking</a></li>
            <li><a href="#"><i class="fa fa-low-vision"></i> AIPT</a></li>
            <li><a href="#"><i class="fa fa-gavel"></i> Verify Certificate</a></li>
            <li><a href="#"><i class="fa fa-archive"></i> Legacy Data</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>UPOV DL Courses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-graduation-cap"></i> Scholarships</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-users"></i> UPOV Participants</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> Countries and Org</a></li>
            <li><a href="#"><i class="fa fa-black-tie"></i> UPOV Representatives</a></li>
            <li><a href="#"><i class="fa fa-usd"></i> UPOV Fee Matrix</a></li>
            <li><a href="#"><i class="fa fa-barcode"></i> UPOV SAR</a></li>
            <li><a href="#"><i class="fa fa-gavel"></i> Verify Certificate</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>PCT DL Courses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-certificate"></i>
            <span>Professional Dev. Program</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-graduation-cap"></i> Scholarships</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-university"></i>
            <span>Academic Inst. Program</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-graduation-cap"></i> Scholarships</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-building-o"></i>
            <span>Summer School</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>IP Expert</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-hand-o-up"></i> Candidates</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Availability Request</a></li>
            <li><a href="#"><i class="fa fa-user-circle-o"></i> Expert Hiring</a></li>
            <li><a href="#"><i class="fa fa-users"></i> IP Experts</a></li>
            <li><a href="#"><i class="fa fa-list-ul"></i> IP Fields</a></li>
            <li><a href="#"><i class="fa fa-bar-chart"></i>Reports</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-id-card-o"></i> Home</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i>
            <span>Stats & Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-line-chart"></i> Statistics and Reports</a></li>
            <li><a href="#"><i class="fa fa-list-alt"></i> Reports</a></li>
            <li><a href="#"><i class="fa fa-line-chart"></i> Statistics</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-wrench"></i>
            <span>Miscellaneous</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-users"></i> Participants</a></li>
            <li><a href="#"><i class="fa fa-wifi"></i> WeLC Availability</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>WIPO Briefing Program</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-book"></i> Briefing Requests</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Registrations</a></li>
            <li><a href="#"><i class="fa fa-graduation-cap"></i> Scholarships</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Variants</a></li>
            <li><a href="#"><i class="fa fa-id-card-o"></i> Instances</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>Payments</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-lightbulb-o"></i>
            <span>Utilities</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-paper-plane"></i> Mass Mailer</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> Resources</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="{{ route('system_settings.index') }}">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('program.index') }}"><i class="fa fa-cubes"></i> Programs</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Courses</a></li>
            <li><a href="#"><i class="fa fa-cogs"></i> Course Categories</a></li>
            <li><a href="#"><i class="fa fa-language"></i> Languages</a></li>
            <li><a href="#"><i class="fa fa-flag-checkered"></i> Countries and Org</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> Regions</a></li>
            <li><a href="#"><i class="fa fa-briefcase"></i> Professions</a></li>
            <li><a href="#"><i class="fa fa-black-tie"></i> Prof. Area</a></li>
            <li><a href="#"><i class="fa fa-industry"></i> Economic Groups</a></li>
            <li><a href="#"><i class="fa fa-money"></i> Fee</a></li>
            <li><a href="#"><i class="fa fa-sitemap"></i> Fee Category</a></li>
            <li><a href="#"><i class="fa fa-cc-visa"></i> Credit Card Transactions</a></li>
            <li><a href="#"><i class="fa fa-file-text-o"></i> Document Type</a></li>
            <li><a href="#"><i class="fa fa-newspaper-o"></i> Brochure & Flyers</a></li>
            <li><a href="#"><i class="fa fa-list-ul"></i> ACRP Logs</a></li>
            <li><a href="#"><i class="fa fa-list-ul"></i> ACAD Logs</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Notifications</a></li>
            <li><a href="#"><i class="fa fa-puzzle-piece"></i> Notificaton Placeholders</a></li>
            <li><a href="#"><i class="fa fa-clock-o"></i> DL Sessions</a></li>
          </ul>
        </li>


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>