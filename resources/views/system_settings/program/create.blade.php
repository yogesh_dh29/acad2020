<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Add New Program</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name" class="control-label">ID</label>
              <input type="text" class="form-control" id="id" name="id">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="control-label">Acronym</label>
              <input type="text" class="form-control" id="acronym" name="acronym">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="control-label">Generic Email</label>
              <input type="text" class="form-control" id="generic_email" name="generic_email">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="control-label">URL</label>
              <input type="text" class="form-control" id="url" name="url">
            </div>
            <div class="box box-info">
              <div class="box-header">
                <!-- /. tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body pad">
                <textarea id="editor1" name="editor1" rows="10" cols="80">
                </textarea>
              </div>
            </div>
            <!-- /.box -->


            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Add</button>
            </div>            
          </form>
        </div>
    </div>
  </div>
</div>
  <!-- /.modal -->