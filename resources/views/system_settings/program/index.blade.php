@extends('layouts.app')

@section('title', 'System Settings - Program')

@section('content')
    <section class="content-header">
      <h1>
        Program
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('homepage.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('system_settings.index') }}">System Settings</a></li>
        <li class="active">Program</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="col-md-6" style="padding-top: 20px;">
              <div class="box-header with-border">
                <h3 class="box-title">Total Records {{ $programs->count() }}</h3>
              </div> 
            </div>   
            <div class="col-md-6">
                <div class="box-body" style="float: right;">
                  <a href="#" class="btn btn-app" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-plus-square"></i> Add Program                     
                  </a>
                  @include('system_settings.program.create')
                  <a class="btn btn-app">
                    <i class="fa fa-file-excel-o"></i> Export
                  </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Program</th>
                  <th>ID</th>
                  <th>Acronym</th>
                  <th>Email</th>
                  <th>URL</th>
                </tr>
                @foreach($programs as $program)
                <tr>
                  <td>{{ Str::before($program->name_en, '##') }}</td>
                  <td>{{ $program->id }}</td>
                  <td>{{ $program->acronym}}</td>
                  <td>{{ $program->generic_email}}</td>
                  <td>{{ $program->url}}</td>
                </tr>
                @endforeach
              </table>
              {{$programs->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  <!-- /.content-wrapper -->
  <!-- Control Sidebar -->
@endsection

@section('javascripts')
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- CK Editor -->
<script src="../../bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
@endsection
