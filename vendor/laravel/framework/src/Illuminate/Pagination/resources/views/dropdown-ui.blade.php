@if ($paginator->hasPages())
<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Dropdown button
  </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="dropdown-item" aria-disabled="true" aria-label="@lang('pagination.previous')"> <i class="left chevron icon"></i> </a>
        @else
            <a class="dropdown-item" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"> <i class="left chevron icon"></i> </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="dropdown-item" aria-disabled="true">{{ $element }}</a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="dropdown-item active" href="{{ $url }}" aria-current="page">{{ $page }}</a>
                    @else
                        <a class="dropdown-item"href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="dropdown-item" href="{{ $paginator->nextPageUrl() }}" ><i class="right chevron icon"></i></a>
        @else
            <a class="dropdown-item" aria-disabled="true" aria-label="@lang('pagination.next')"> <i class="right chevron icon"></i></a>
        @endif
    </div>
@endif
