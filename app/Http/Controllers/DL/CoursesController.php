<?php

namespace App\Http\Controllers\DL;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use DataTables;
use App\Models\RpDlUpovRegistrationView;
use App\Models\Language;
use App\Models\Course;
use App\Models\Program;
use App\Models\CourseVariant;
use App\Models\CourseInstance;
use App\Models\EcoGroup;
use App\Models\Region;
use App\Models\Country;
use App\Models\DlFeeCategory;
use App\Models\ProfessionalArea;
use App\DatabaseLibrary\DatabaseColumns;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->where('ci_course_start', '>', '2020-08-01')->paginate(50);

        //For All
         // $dlregis = RpDlUpovRegistrationView::
         //            select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->paginate(50);

        //RenderConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('6','4'))->paginate(50);

        //RenderNonConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','5'))->paginate(50);

        //RenderExpired
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','3','4','5','6'))->paginate(50);

        //RenderCancelled
         // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('4','6'))->paginate(50);

        // $dlregis = DB::select('select * from RpDlUpovRegistrationView WHERE ROWNUM < 10000');

        // $dlregis2 = RpDlUpovRegistrationView::limit(5)->get();        

        // $dlregis = RpDlUpovRegistrationView::
        //             select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('co_program_id',array('1','2','7'))->paginate(20);

        $language = cache()->remember('language', 60*60*24, function() { 
            return Language::orderBy('name', 'asc')->pluck('name', 'id');
        });
            
        $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
            return Program::select('id','acronym','name_en')
                ->with(['courses' => function($query1) {
                    $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                        $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                    }]);
                }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                ->get();
        });

        $country = cache()->remember('country', 60*60*24, function() { 
            return Country::with(['eco_group', 'region'])
                                    ->where('is_intergov_org', 0)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });

        $nationality = cache()->remember('nationality', 60*60*24, function() {
            return Country::with(['eco_group', 'region'])
                                    ->where('is_nationality', 1)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });                                   

        $bureau = cache()->remember('bureau', 60*60*24, function() {
            return Region::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });

        $eco_group = cache()->remember('eco_group', 60*60*24, function() {
            return EcoGroup::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });                        
                                                          
        $professional_area =  cache()->remember('professional_area', 60*60*24, function() {
            return DlFeeCategory::with(['professional_areas' => function($quer) {
                                    $quer->orderBy('name_en','asc');
                                }])->get();
        });    

        // $onebigcourse = Program::
        //                     with(['courses' => function($query1) {
        //                         $query1->with(['variants' => function($query2) {
        //                             $query2->with('instances')->orderBy('acronym','asc');
        //                         }]);
        //                     }])->whereIn('acronym', array('DL','UPOV','JTIP'))
        //                     ->get();                            
        if ($request->ajax()) {                       
            $data = Course::select('id','acronym','is_paying','admin_email','currency_code','course_type','is_open','duration','has_certificate','serie','is_special','is_cmo_enabled','ip_field')->whereIn('program_id',array('1'));       

            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
            $btn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
            return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);

        }
        return view('dl.courses.index', compact('language','onebigcourse','country','nationality','bureau','eco_group','professional_area'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
