<?php

namespace App\Http\Controllers\Dl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use DataTables;
use App\Models\RpDlUpovRegistrationView;
use App\Models\Language;
use App\Models\Course;
use App\Models\Program;
use App\Models\CourseVariant;
use App\Models\CourseInstance;
use App\Models\EcoGroup;
use App\Models\Region;
use App\Models\Country;
use App\Models\DlFeeCategory;
use App\Models\ProfessionalArea;
use App\DatabaseLibrary\DatabaseColumns;

class RegistrationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('dl.home');
    }
    public function index(Request $request)
    {   
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->where('ci_course_start', '>', '2020-08-01')->paginate(50);

        //For All
         // $dlregis = RpDlUpovRegistrationView::
         //            select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->paginate(50);

        //RenderConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('6','4'))->paginate(50);

        //RenderNonConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','5'))->paginate(50);

        //RenderExpired
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','3','4','5','6'))->paginate(50);

        //RenderCancelled
         // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('4','6'))->paginate(50);

        // $dlregis = DB::select('select * from RpDlUpovRegistrationView WHERE ROWNUM < 10000');

        // $dlregis2 = RpDlUpovRegistrationView::limit(5)->get();        

        // $dlregis = RpDlUpovRegistrationView::
        //             select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('co_program_id',array('1','2','7'))->paginate(20);

        $language = cache()->remember('language', 60*60*24, function() { 
            return Language::orderBy('name', 'asc')->pluck('name', 'id');
        });
            
        $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
            return Program::select('id','acronym','name_en')
                ->with(['courses' => function($query1) {
                    $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                        $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                    }]);
                }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                ->get();
        });

        $country = cache()->remember('country', 60*60*24, function() { 
            return Country::with(['eco_group', 'region'])
                                    ->where('is_intergov_org', 0)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });

        $nationality = cache()->remember('nationality', 60*60*24, function() {
            return Country::with(['eco_group', 'region'])
                                    ->where('is_nationality', 1)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });                                   

        $bureau = cache()->remember('bureau', 60*60*24, function() {
            return Region::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });

        $eco_group = cache()->remember('eco_group', 60*60*24, function() {
            return EcoGroup::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });                        
                                                          
        $professional_area =  cache()->remember('professional_area', 60*60*24, function() {
            return DlFeeCategory::with(['professional_areas' => function($quer) {
                                    $quer->orderBy('name_en','asc');
                                }])->get();
        });    

        // $onebigcourse = Program::
        //                     with(['courses' => function($query1) {
        //                         $query1->with(['variants' => function($query2) {
        //                             $query2->with('instances')->orderBy('acronym','asc');
        //                         }]);
        //                     }])->whereIn('acronym', array('DL','UPOV','JTIP'))
        //                     ->get();                            
        if ($request->ajax()) {                       
            $data = RpDlUpovRegistrationView::
                    select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code','lang_id')->whereIn('co_program_id',array('1','2','7'));

            switch ($request->get('targett')) {
                case 'RenderConfirmed':
                    $data->whereIn('reg_status',array('6','4'));
                break;
                case 'RenderPending':
                    $data->whereIn('reg_status',array('0','5'));
                break;
                case 'RenderCancelled':
                    $data->whereIn('reg_status',array('4','6'));
                break;
                case 'RenderAllCurrent':
                    $data->where('reg_date_create', '>=', '2019-01-01');
                break;
                case 'RenderAll':
                    $data->whereIn('reg_status',array('6','4'));
                break;
                case 'RenderExpired':
                    $data->whereIn('reg_status',array('0','3','4','5','6'));
                break;
                
                default:
                # code...
                break;
            }            
            // if($request->get('registration_type') == 'RenderConfirmed'){
            //     $data->whereIn('reg_status',array('6','4'));
            // }       
            if($request->get('language')){
                $data->where('lang_id',$request->get('language'));
            }
            if($request->get('course_acronym')){
                $data->where('co_acronym',$request->get('course_acronym'));
            }
            if($request->get('course_variants')){
                $data->where('cv_acronym',$request->get('course_variants'));
            }
            if($request->get('course_code')){
                $data->where('ci_acronym',$request->get('course_code'));
            }
            if($request->get('country')){
                $data->where('client_country',$request->get('country'));
            }            
            if($request->get('nationality')){
                $data->where('client_nationality',$request->get('nationality'));
            }
            if($request->get('bureau')){
                $data->where('ci_acronym',$request->get('bureau'));
            }
            if($request->get('eco_group')){
                $data->where('eco_group',$request->get('eco_group'));
            }
            if($request->get('gender')){
                $data->where('client_gender',$request->get('gender'));
            }
            if($request->get('occupation')){
                $data->where('client_occupation',$request->get('occupation'));
            }
            if($request->get('prof_area')){
                $data->where('lang_id',$request->get('prof_area'));
            }
            if($request->get('confirmation')){
                $data->where('co_is_paying',$request->get('confirmation'));
            }
            if($request->get('cmo')){
                $data->where('is_cmo_enabled',$request->get('cmo'));
            }
            if($request->get('is_ext_reg')){
                $data->where('lang_id',$request->get('is_ext_reg'));
            }
            if($request->get('payment_method')){
                $data->where('ci_id',$request->get('payment_method'));
            }
            // if($request->get('chkbox_upov')){
            //     $data->where('ci_acronym',$request->get('chkbox_upov'));
            // }
            // if($request->get('chkbox_jtip')){
            //     $data->where('ci_acronym',$request->get('chkbox_jtip'));
            // }            

            $table = Datatables::of($data);
            
            return $table->make(true);

        }
        return view('dl.index', compact('language','onebigcourse','country','nationality','bureau','eco_group','professional_area'));
    }
    public function indexx(Request $request)
    {
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->where('ci_course_start', '>', '2020-08-01')->paginate(50);

        //For All
         // $dlregis = RpDlUpovRegistrationView::
         //            select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->paginate(50);

        //RenderConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('6','4'))->paginate(50);

        //RenderNonConfirmed
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','5'))->paginate(50);

        //RenderExpired
        // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('0','3','4','5','6'))->paginate(50);

        //RenderCancelled
         // $dlregis = RpDlUpovRegistrationView::select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('reg_status',array('4','6'))->paginate(50);

        // $dlregis = DB::select('select * from RpDlUpovRegistrationView WHERE ROWNUM < 10000');

        // $dlregis2 = RpDlUpovRegistrationView::limit(5)->get();        

        // $dlregis = RpDlUpovRegistrationView::
        //             select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('co_program_id',array('1','2','7'))->paginate(20);

        $language = cache()->remember('language', 60*60*24, function() { 
            return Language::orderBy('name', 'asc')->pluck('name', 'id');
        });
            
        $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
            return Program::select('id','acronym','name_en')
                ->with(['courses' => function($query1) {
                    $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                        $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                    }]);
                }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                ->get();
        });

        $country = cache()->remember('country', 60*60*24, function() { 
            return Country::with(['eco_group', 'region'])
                                    ->where('is_intergov_org', 0)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });

        $nationality = cache()->remember('nationality', 60*60*24, function() {
            return Country::with(['eco_group', 'region'])
                                    ->where('is_nationality', 1)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });                                   

        $bureau = cache()->remember('bureau', 60*60*24, function() {
            return Region::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });

        $eco_group = cache()->remember('eco_group', 60*60*24, function() {
            return EcoGroup::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });                        
                                                          
        $professional_area =  cache()->remember('professional_area', 60*60*24, function() {
            return DlFeeCategory::with(['professional_areas' => function($quer) {
                                    $quer->orderBy('name_en','asc');
                                }])->get();
        });    

        // $onebigcourse = Program::
        //                     with(['courses' => function($query1) {
        //                         $query1->with(['variants' => function($query2) {
        //                             $query2->with('instances')->orderBy('acronym','asc');
        //                         }]);
        //                     }])->whereIn('acronym', array('DL','UPOV','JTIP'))
        //                     ->get();                            
        if ($request->ajax()) {                       
            $data = RpDlUpovRegistrationView::
                    select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code','lang_id')->whereIn('co_program_id',array('1','2','7'));

            switch ($request->get('targett')) {
                case 'RenderConfirmed':
                    $data->whereIn('reg_status',array('6','4'));
                break;
                case 'RenderPending':
                    $data->whereIn('reg_status',array('0','5'));
                break;
                case 'RenderCancelled':
                    $data->whereIn('reg_status',array('4','6'));
                break;
                case 'RenderAllCurrent':
                    $data->where('reg_date_create', '>=', '2019-01-01');
                break;
                case 'RenderAll':
                    $data->whereIn('reg_status',array('6','4'));
                break;
                case 'RenderExpired':
                    $data->whereIn('reg_status',array('0','3','4','5','6'));
                break;
                
                default:
                # code...
                break;
            }            
            // if($request->get('registration_type') == 'RenderConfirmed'){
            //     $data->whereIn('reg_status',array('6','4'));
            // }       
            if($request->get('language')){
                $data->where('lang_id',$request->get('language'));
            }
            if($request->get('course_acronym')){
                $data->where('co_acronym',$request->get('course_acronym'));
            }
            if($request->get('course_variants')){
                $data->where('cv_acronym',$request->get('course_variants'));
            }
            if($request->get('course_code')){
                $data->where('ci_acronym',$request->get('course_code'));
            }
            if($request->get('country')){
                $data->where('client_country',$request->get('country'));
            }            
            if($request->get('nationality')){
                $data->where('client_nationality',$request->get('nationality'));
            }
            if($request->get('bureau')){
                $data->where('ci_acronym',$request->get('bureau'));
            }
            if($request->get('eco_group')){
                $data->where('eco_group',$request->get('eco_group'));
            }
            if($request->get('gender')){
                $data->where('client_gender',$request->get('gender'));
            }
            if($request->get('occupation')){
                $data->where('client_occupation',$request->get('occupation'));
            }
            if($request->get('prof_area')){
                $data->where('lang_id',$request->get('prof_area'));
            }
            if($request->get('confirmation')){
                $data->where('co_is_paying',$request->get('confirmation'));
            }
            if($request->get('cmo')){
                $data->where('is_cmo_enabled',$request->get('cmo'));
            }
            if($request->get('is_ext_reg')){
                $data->where('lang_id',$request->get('is_ext_reg'));
            }
            if($request->get('payment_method')){
                $data->where('ci_id',$request->get('payment_method'));
            }
            // if($request->get('chkbox_upov')){
            //     $data->where('ci_acronym',$request->get('chkbox_upov'));
            // }
            // if($request->get('chkbox_jtip')){
            //     $data->where('ci_acronym',$request->get('chkbox_jtip'));
            // }            

                return Datatables::of($data)
                    ->addIndexColumn()
                    ->make(true);
        }
        return view('dl.indexx', compact('language','onebigcourse','country','nationality','bureau','eco_group','professional_area'));        
    }
    public function indexxx(Request $request)
    {                
        $language = cache()->remember('language', 60*60*24, function() { 
            return Language::orderBy('name', 'asc')->pluck('name', 'id');
        });
            
        $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
            return Program::select('id','acronym','name_en')
                ->with(['courses' => function($query1) {
                    $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                        $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                    }]);
                }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                ->get();
        });

        $country = cache()->remember('country', 60*60*24, function() { 
            return Country::with(['eco_group', 'region'])
                                    ->where('is_intergov_org', 0)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });

        $nationality = cache()->remember('nationality', 60*60*24, function() {
            return Country::with(['eco_group', 'region'])
                                    ->where('is_nationality', 1)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');
        });                                   

        $bureau = cache()->remember('bureau', 60*60*24, function() {
            return Region::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });

        $eco_group = cache()->remember('eco_group', 60*60*24, function() {
            return EcoGroup::orderBy('id', 'asc')
                                ->pluck('name_en','id');
        });                        
                                                          
        $professional_area =  cache()->remember('professional_area', 60*60*24, function() {
            return DlFeeCategory::with(['professional_areas' => function($quer) {
                                    $quer->orderBy('name_en','asc');
                                }])->get();
        });

        
        $d = $request->all();

        $data = RpDlUpovRegistrationView::
        select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code','lang_id')->whereIn('co_program_id',array('1','2','7'));

        switch ($request->registration_type) {
            case 'RenderConfirmed':
                $data->whereIn('reg_status',array('6','4'));
            break;
            case 'RenderPending':
                $data->whereIn('reg_status',array('0','5'));
            break;
            case 'RenderCancelled':
                $data->whereIn('reg_status',array('4','6'));
            break;
            case 'RenderAllCurrent':
                $data->where('reg_date_create', '>=', '2019-01-01');
            break;
            case 'RenderAll':
                $data->whereIn('reg_status',array('6','4'));
            break;
            case 'RenderExpired':
                $data->whereIn('reg_status',array('0','3','4','5','6'));
            break;
            
            default:
            # code...
            break;
        }            

        if($request->language){
            $data = $data->where('lang_id',$request->language);    
        }
        if($request->course_acronym){
            $data->where('co_acronym',$request->course_acronym);
        }
        if($request->course_variants){
            $data->where('cv_acronym',$request->course_variants);
        }
        if($request->course_code){
            $data->where('ci_acronym',$request->course_code);
        }
        if($request->country){
            $data->where('client_country',$request->country);
        }            
        if($request->nationality){
            $data->where('client_nationality',$request->nationality);
        }
        if($request->bureau){
            $data->where('ci_acronym',$request->bureau);
        }
        if($request->eco_group){
            $data->where('eco_group',$request->eco_group);
        }
        if($request->gender){
            $data->where('client_gender',$request->gender);
        }
        if($request->occupation){
            $data->where('client_occupation',$request->occupation);
        }
        if($request->prof_area){
            $data->where('lang_id',$request->prof_area);
        }
        if($request->confirmation){
            $data->where('co_is_paying',$request->confirmation);
        }
        if($request->cmo){
            $data->where('is_cmo_enabled',$request->cmo);
        }
        if($request->is_ext_reg){
            $data->where('lang_id',$request->is_ext_reg);
        }
        if($request->payment_method){
            $data->where('ci_id',$request->payment_method);
        }                
        $data = $data->orderBy('reg_id', 'desc')->paginate(50);

        //dd($request);

        return view('dl.indexxx', compact('data','language','onebigcourse','country','nationality','bureau','eco_group','professional_area','d'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getVariants(Request $request) 
    {
        
        if($request->acronym)
        {
            $course_id = Course::select('id')->where('acronym', $request->acronym)->first();

            $passVariants = CourseVariant::where('course_id', $course_id->id)->orderBy('acronym', 'asc')->pluck('acronym', 'id');
                                   
            return response()->json($passVariants);
        }
        else
        {
            $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
                return Program::select('id','acronym','name_en')
                    ->with(['courses' => function($query1) {
                        $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                            $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                        }]);
                    }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                    ->get();
            });

            return response()->json($onebigcourse);
        }
    }

    public function getInstances(Request $request) 
    {
        if($request->variant == "« Variants »")
        {
            
            $onebigcourse = cache()->remember('onebigcourse', 60*60*24, function() {
                return Program::select('id','acronym','name_en')
                    ->with(['courses' => function($query1) {
                        $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                            $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
                        }]);
                    }])->whereIn('acronym', array('DL','UPOV','JTIP'))
                    ->get();
            });

            return response()->json($onebigcourse);
        }
        else
        {
            $variant_id = CourseVariant::select('id')->where('acronym', $request->variant)->first();

            $passInstances = CourseInstance::where('course_variant_id', $variant_id->id)->orderBy('acronym', 'asc')->pluck('acronym', 'id');
                                   
            return response()->json($passInstances);
        }
    }    
}
