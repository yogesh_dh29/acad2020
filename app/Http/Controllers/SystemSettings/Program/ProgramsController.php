<?php

namespace App\Http\Controllers\SystemSettings\Program;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Program;

class ProgramsController extends Controller
{
    //
    public function index(){

    	$programs = Program::paginate(10);
        
    	return view('system_settings.program.index',compact('programs'));
    }

    public function create(){
    	return view('system_settings.program.create');
    }
}
