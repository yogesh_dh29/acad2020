<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\RpDlUpovRegistrationView;
use App\Models\Language;
use App\Models\Course;
use App\Models\Program;
use App\Models\CourseVariant;
use App\Models\CourseInstance;
use App\Models\EcoGroup;
use App\Models\Region;
use App\Models\Country;
use App\Models\DlFeeCategory;
use App\Models\ProfessionalArea;
use App\DatabaseLibrary\DatabaseColumns;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        //For All
         $dlregis = RpDlUpovRegistrationView::
                    select('reg_id','reg_date_create','ci_acronym','client_full_name','client_email','client_occupation','reg_country','reg_nationality','ci_passing_grades','reg_currency_code')->whereIn('co_program_id',array('1','2','7'))->paginate(50);

        $language = Language::orderBy('name', 'asc')->pluck('name', 'id');

        $onebigcourse = Program::select('id','acronym','name_en')
        ->with(['courses' => function($query1) {
            $query1->select('id','program_id','acronym')->with(['variants' => function($query2) {
                $query2->select('id', 'course_id','language_id','acronym')->with('instances')->orderBy('acronym','asc');
            }]);
        }])->whereIn('acronym', array('DL','UPOV','JTIP'))
        ->get();

        $country = Country::with(['eco_group', 'region'])
                                    ->where('is_intergov_org', 0)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');

        $nationality = Country::with(['eco_group', 'region'])
                                    ->where('is_nationality', 1)
                                    ->orderBy('name_en','asc')
                                    ->pluck('name_en', 'id');

        $bureau = Region::orderBy('id', 'asc')
                                ->pluck('name_en','id');

        $eco_group = EcoGroup::orderBy('id', 'asc')
                                ->pluck('name_en','id');                        
                                                          
        $professional_area = DlFeeCategory::with(['professional_areas' => function($quer) {
                                    $quer->orderBy('name_en','asc');
                                }])->get();

        // $onebigcourse = Program::
        //                     with(['courses' => function($query1) {
        //                         $query1->with(['variants' => function($query2) {
        //                             $query2->with('instances')->orderBy('acronym','asc');
        //                         }]);
        //                     }])->whereIn('acronym', array('DL','UPOV','JTIP'))
        //                     ->get();                            
 
        return view('dl.indexx', compact('dlregis','language','onebigcourse','country','nationality','bureau','eco_group','professional_area'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}
