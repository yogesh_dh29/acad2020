<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DlFeeCategory extends Model
{
    //
    protected $table = 'RP_DL_FEE_CATEGORY';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function professional_areas()
	{
		return $this->hasMany('App\Models\ProfessionalArea', 'fee_category', 'id');
	}
}
