<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EcoGroup extends Model
{
    //
    protected $table = 'RP_ECO_GROUP';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function countries()
	{
		return $this->hasMany('App\Models\Country', 'eco_group', 'id');
	}
}
