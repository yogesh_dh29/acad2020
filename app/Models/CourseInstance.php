<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseInstance extends Model
{
    //
    protected $table = 'RP_COURSE_INSTANCE';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function variant()
    {
    	return $this->belongsTo('App\Models\CourseVariant', 'course_id' , 'id');
    }
}
