<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    protected $table = 'RP_PROGRAM';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;    
	protected $fillable = [];    

	public function courses()
	{
		return $this->hasMany('App\Models\Course', 'program_id', 'id');
	}
}
