<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseVariant extends Model
{
    //
    protected $table = 'RP_COURSE_VARIANT';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function course()
    {
    	return $this->belongsTo('App\Models\Course', 'course_id' , 'id');
    }

    public function instances()
    {
        return $this->hasMany('App\Models\CourseInstance', 'course_variant_id', 'id');
    }
}
