<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'RP_COUNTRY';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function eco_group()
    {
    	return $this->belongsTo('App\Models\EcoGroup', 'eco_group' , 'id');
    }

    public function region()
    {
    	return $this->belongsTo('App\Models\Region', 'region' , 'id');
    }
}
