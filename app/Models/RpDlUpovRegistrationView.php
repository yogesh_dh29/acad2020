<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RpDlUpovRegistrationView extends Model
{
    //
    protected $connection = 'oracle';
    protected $table = 'RP_DL_UPOV_REGISTRATION_VIEW';
    protected $primaryKey = 'REG_ID';   
    public $timestamps = false;
    public $incrementing = false;
}
