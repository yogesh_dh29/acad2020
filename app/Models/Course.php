<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    protected $table = 'RP_COURSE';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function program()
    {
    	return $this->belongsTo('App\Models\Program', 'program_id' , 'id');
    }

    public function variants()
    {
        return $this->hasMany('App\Models\CourseVariant', 'course_id', 'id');
    }
}
