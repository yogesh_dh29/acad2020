<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessionalArea extends Model
{
    //
    protected $table = 'RP_PROFESSIONAL_AREA';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [];

    public function dl_fee_category()
    {
    	return $this->belongsTo('App\Models\DlFeeCategory', 'fee_category' , 'id');
    }
}
